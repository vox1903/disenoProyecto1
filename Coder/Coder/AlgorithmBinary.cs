﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Coder
{
    class AlgorithmBinary : Algorithm
    {

        public AlgorithmBinary(String pName)
        {
            this.name = pName;
        }

        public override string decode(String value)
        {
            string language = "abcdefghijklmnopqrstuvwxyz";
            string message = "";
            foreach (String s in value.Split(' '))
            {
                if (s == "*")
                {
                    message += " ";
                }
                else
                {
                    message += language.ElementAt(Convert.ToInt32(s, 2));
                }

            }
            return message;
        }

        public override string encode(String value)
        {
            string language = "abcdefghijklmnopqrstuvwxyz";
            string message = "";
            foreach (char c in value.ToLower())
            {
                if (c == ' ')
                {
                    message += "* ";
                }
                else
                {

                    message += (Convert.ToString(language.IndexOf(c), 2)).PadLeft(5, '0') + " ";
                }


            }
            if (message.Last() == ' ')
            {
                message = message.Substring(0, message.Length-1 );
            }
            return message;
        }
    }
}
