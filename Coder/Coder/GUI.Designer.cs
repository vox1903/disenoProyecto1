﻿namespace Coder
{
    partial class GUI
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.comboBoxKeyWord = new System.Windows.Forms.ComboBox();
            this.comboBoxPhone = new System.Windows.Forms.ComboBox();
            this.comboBoxLetter = new System.Windows.Forms.ComboBox();
            this.comboBoxBinary = new System.Windows.Forms.ComboBox();
            this.checkBoxPhone = new System.Windows.Forms.CheckBox();
            this.checkBoxLetter = new System.Windows.Forms.CheckBox();
            this.checkBoxKeyWord = new System.Windows.Forms.CheckBox();
            this.label2 = new System.Windows.Forms.Label();
            this.checkBoxBinary = new System.Windows.Forms.CheckBox();
            this.textBoxKeyword = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.richTextBoxInput = new System.Windows.Forms.RichTextBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.richTextBoxResults = new System.Windows.Forms.RichTextBox();
            this.buttonAccept = new System.Windows.Forms.Button();
            this.buttonClear = new System.Windows.Forms.Button();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.checkBoxXml = new System.Windows.Forms.CheckBox();
            this.checkBoxTxt = new System.Windows.Forms.CheckBox();
            this.checkBoxPdf = new System.Windows.Forms.CheckBox();
            this.button3 = new System.Windows.Forms.Button();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.comboBoxKeyWord);
            this.groupBox2.Controls.Add(this.comboBoxPhone);
            this.groupBox2.Controls.Add(this.comboBoxLetter);
            this.groupBox2.Controls.Add(this.comboBoxBinary);
            this.groupBox2.Controls.Add(this.checkBoxPhone);
            this.groupBox2.Controls.Add(this.checkBoxLetter);
            this.groupBox2.Controls.Add(this.checkBoxKeyWord);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.checkBoxBinary);
            this.groupBox2.Controls.Add(this.textBoxKeyword);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Controls.Add(this.richTextBoxInput);
            this.groupBox2.Location = new System.Drawing.Point(12, 232);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(537, 264);
            this.groupBox2.TabIndex = 0;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Input";
            this.groupBox2.Enter += new System.EventHandler(this.groupBox2_Enter);
            // 
            // comboBoxKeyWord
            // 
            this.comboBoxKeyWord.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxKeyWord.FormattingEnabled = true;
            this.comboBoxKeyWord.Items.AddRange(new object[] {
            "Encode",
            "Decode"});
            this.comboBoxKeyWord.Location = new System.Drawing.Point(155, 219);
            this.comboBoxKeyWord.Name = "comboBoxKeyWord";
            this.comboBoxKeyWord.Size = new System.Drawing.Size(107, 21);
            this.comboBoxKeyWord.TabIndex = 16;
            // 
            // comboBoxPhone
            // 
            this.comboBoxPhone.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxPhone.FormattingEnabled = true;
            this.comboBoxPhone.Items.AddRange(new object[] {
            "Encode",
            "Decode"});
            this.comboBoxPhone.Location = new System.Drawing.Point(155, 192);
            this.comboBoxPhone.Name = "comboBoxPhone";
            this.comboBoxPhone.Size = new System.Drawing.Size(107, 21);
            this.comboBoxPhone.TabIndex = 15;
            // 
            // comboBoxLetter
            // 
            this.comboBoxLetter.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxLetter.FormattingEnabled = true;
            this.comboBoxLetter.Items.AddRange(new object[] {
            "Encode",
            "Decode"});
            this.comboBoxLetter.Location = new System.Drawing.Point(155, 165);
            this.comboBoxLetter.Name = "comboBoxLetter";
            this.comboBoxLetter.Size = new System.Drawing.Size(107, 21);
            this.comboBoxLetter.TabIndex = 14;
            // 
            // comboBoxBinary
            // 
            this.comboBoxBinary.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxBinary.FormattingEnabled = true;
            this.comboBoxBinary.Items.AddRange(new object[] {
            "Encode",
            "Decode"});
            this.comboBoxBinary.Location = new System.Drawing.Point(155, 138);
            this.comboBoxBinary.Name = "comboBoxBinary";
            this.comboBoxBinary.Size = new System.Drawing.Size(107, 21);
            this.comboBoxBinary.TabIndex = 13;
            // 
            // checkBoxPhone
            // 
            this.checkBoxPhone.AutoSize = true;
            this.checkBoxPhone.Location = new System.Drawing.Point(10, 194);
            this.checkBoxPhone.Name = "checkBoxPhone";
            this.checkBoxPhone.Size = new System.Drawing.Size(106, 17);
            this.checkBoxPhone.TabIndex = 12;
            this.checkBoxPhone.Text = "Phone Algorithm:";
            this.checkBoxPhone.UseVisualStyleBackColor = true;
            this.checkBoxPhone.CheckedChanged += new System.EventHandler(this.checkBoxPhone_CheckedChanged);
            // 
            // checkBoxLetter
            // 
            this.checkBoxLetter.AutoSize = true;
            this.checkBoxLetter.Location = new System.Drawing.Point(10, 167);
            this.checkBoxLetter.Name = "checkBoxLetter";
            this.checkBoxLetter.Size = new System.Drawing.Size(102, 17);
            this.checkBoxLetter.TabIndex = 11;
            this.checkBoxLetter.Text = "Letter Algorithm:";
            this.checkBoxLetter.UseVisualStyleBackColor = true;
            this.checkBoxLetter.CheckedChanged += new System.EventHandler(this.checkBoxLetter_CheckedChanged);
            // 
            // checkBoxKeyWord
            // 
            this.checkBoxKeyWord.AutoSize = true;
            this.checkBoxKeyWord.Location = new System.Drawing.Point(10, 221);
            this.checkBoxKeyWord.Name = "checkBoxKeyWord";
            this.checkBoxKeyWord.Size = new System.Drawing.Size(122, 17);
            this.checkBoxKeyWord.TabIndex = 10;
            this.checkBoxKeyWord.Text = "Key Word Algorithm:";
            this.checkBoxKeyWord.UseVisualStyleBackColor = true;
            this.checkBoxKeyWord.CheckedChanged += new System.EventHandler(this.checkBoxKeyWord_CheckedChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(281, 222);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(51, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Keyword:";
            // 
            // checkBoxBinary
            // 
            this.checkBoxBinary.AutoSize = true;
            this.checkBoxBinary.Location = new System.Drawing.Point(10, 140);
            this.checkBoxBinary.Name = "checkBoxBinary";
            this.checkBoxBinary.Size = new System.Drawing.Size(104, 17);
            this.checkBoxBinary.TabIndex = 9;
            this.checkBoxBinary.Text = "Binary Algorithm:";
            this.checkBoxBinary.UseVisualStyleBackColor = true;
            this.checkBoxBinary.CheckedChanged += new System.EventHandler(this.checkBoxBinary_CheckedChanged);
            // 
            // textBoxKeyword
            // 
            this.textBoxKeyword.Enabled = false;
            this.textBoxKeyword.Location = new System.Drawing.Point(338, 219);
            this.textBoxKeyword.Name = "textBoxKeyword";
            this.textBoxKeyword.Size = new System.Drawing.Size(193, 20);
            this.textBoxKeyword.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 119);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(53, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Algorithm:";
            // 
            // richTextBoxInput
            // 
            this.richTextBoxInput.Location = new System.Drawing.Point(6, 17);
            this.richTextBoxInput.Name = "richTextBoxInput";
            this.richTextBoxInput.Size = new System.Drawing.Size(525, 93);
            this.richTextBoxInput.TabIndex = 0;
            this.richTextBoxInput.Text = "";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.richTextBoxResults);
            this.groupBox3.Location = new System.Drawing.Point(12, 28);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(545, 198);
            this.groupBox3.TabIndex = 1;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Results";
            // 
            // richTextBoxResults
            // 
            this.richTextBoxResults.Location = new System.Drawing.Point(6, 19);
            this.richTextBoxResults.Name = "richTextBoxResults";
            this.richTextBoxResults.ReadOnly = true;
            this.richTextBoxResults.Size = new System.Drawing.Size(531, 173);
            this.richTextBoxResults.TabIndex = 0;
            this.richTextBoxResults.Text = ">";
            // 
            // buttonAccept
            // 
            this.buttonAccept.Location = new System.Drawing.Point(429, 600);
            this.buttonAccept.Name = "buttonAccept";
            this.buttonAccept.Size = new System.Drawing.Size(114, 37);
            this.buttonAccept.TabIndex = 4;
            this.buttonAccept.Text = "Accept";
            this.buttonAccept.UseVisualStyleBackColor = true;
            this.buttonAccept.Click += new System.EventHandler(this.buttonAccept_Click);
            // 
            // buttonClear
            // 
            this.buttonClear.Location = new System.Drawing.Point(132, 600);
            this.buttonClear.Name = "buttonClear";
            this.buttonClear.Size = new System.Drawing.Size(114, 37);
            this.buttonClear.TabIndex = 6;
            this.buttonClear.Text = "Clear";
            this.buttonClear.UseVisualStyleBackColor = true;
            this.buttonClear.Click += new System.EventHandler(this.buttonClear_Click);
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.checkBoxXml);
            this.groupBox4.Controls.Add(this.checkBoxTxt);
            this.groupBox4.Controls.Add(this.checkBoxPdf);
            this.groupBox4.Location = new System.Drawing.Point(12, 496);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(537, 98);
            this.groupBox4.TabIndex = 4;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Output Files";
            // 
            // checkBoxXml
            // 
            this.checkBoxXml.AutoSize = true;
            this.checkBoxXml.Location = new System.Drawing.Point(6, 65);
            this.checkBoxXml.Name = "checkBoxXml";
            this.checkBoxXml.Size = new System.Drawing.Size(78, 17);
            this.checkBoxXml.TabIndex = 14;
            this.checkBoxXml.Text = "Xml Format";
            this.checkBoxXml.UseVisualStyleBackColor = true;
            // 
            // checkBoxTxt
            // 
            this.checkBoxTxt.AutoSize = true;
            this.checkBoxTxt.Location = new System.Drawing.Point(6, 42);
            this.checkBoxTxt.Name = "checkBoxTxt";
            this.checkBoxTxt.Size = new System.Drawing.Size(76, 17);
            this.checkBoxTxt.TabIndex = 13;
            this.checkBoxTxt.Text = "Txt Format";
            this.checkBoxTxt.UseVisualStyleBackColor = true;
            // 
            // checkBoxPdf
            // 
            this.checkBoxPdf.AutoSize = true;
            this.checkBoxPdf.Location = new System.Drawing.Point(6, 19);
            this.checkBoxPdf.Name = "checkBoxPdf";
            this.checkBoxPdf.Size = new System.Drawing.Size(77, 17);
            this.checkBoxPdf.TabIndex = 13;
            this.checkBoxPdf.Text = "Pdf Format";
            this.checkBoxPdf.UseVisualStyleBackColor = true;
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(12, 600);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(114, 37);
            this.button3.TabIndex = 0;
            this.button3.Text = "Cancel";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // GUI
            // 
            this.ClientSize = new System.Drawing.Size(565, 649);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.buttonClear);
            this.Controls.Add(this.buttonAccept);
            this.Controls.Add(this.groupBox2);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "GUI";
            this.Load += new System.EventHandler(this.GUI_Load);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.RichTextBox richTextBoxInput;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button buttonAccept;
        private System.Windows.Forms.Button buttonClear;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.RichTextBox richTextBoxResults;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBoxKeyword;
        private System.Windows.Forms.CheckBox checkBoxPhone;
        private System.Windows.Forms.CheckBox checkBoxLetter;
        private System.Windows.Forms.CheckBox checkBoxKeyWord;
        private System.Windows.Forms.CheckBox checkBoxBinary;
        private System.Windows.Forms.CheckBox checkBoxXml;
        private System.Windows.Forms.CheckBox checkBoxTxt;
        private System.Windows.Forms.CheckBox checkBoxPdf;
        private System.Windows.Forms.ComboBox comboBoxKeyWord;
        private System.Windows.Forms.ComboBox comboBoxPhone;
        private System.Windows.Forms.ComboBox comboBoxLetter;
        private System.Windows.Forms.ComboBox comboBoxBinary;
    }
}