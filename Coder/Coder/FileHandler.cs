﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Coder
{
    class FileHandler
    {
        List<Editor> editors;

        public FileHandler()
        {
            editors = new List<Editor>();
            editors.Add(new EditorTxt("Txt"));
            editors.Add(new EditorPdf("Pdf"));
            editors.Add(new EditorXml("Xml"));
        }

        public void writeFile(List<String> content, String fileFormat){
            foreach (Editor editor in editors)
            {
                if (editor.getName() == fileFormat)
                {
                    editor.writeFile(content);
                }
            }
        }

    }
}
