﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Coder
{
    class StringEncoder
    {
        private List<Algorithm> algorithms;
        public StringEncoder(List<Algorithm> pAlgorithms)
        {   
            algorithms = pAlgorithms;
        }

        public String encode(String algorithm, String value)
        {
            foreach (Algorithm a in algorithms)
            {
                if (a.getName() == algorithm)
                {
                    return a.encode(value);
                }
            }
            return null;
        }

        public String decode(String algorithm, String value)
        {
            foreach (Algorithm a in algorithms)
            {
                if (a.getName() == algorithm)
                {
                    return a.decode(value);
                }
            }
            return null;
        }


        public String encode(String algorithm, String value,String parameters)
        {
            foreach (Algorithm a in algorithms)
            {
                if (a.getName() == algorithm & a is AlgorithmWithParameters)
                {
                    AlgorithmWithParameters ap = (AlgorithmWithParameters)a;
                    ap.addParameters(parameters);
                    return a.encode(value);
                }
            }
            return null;
        }

        public String decode(String algorithm, String value, String parameters)
        {
            foreach (Algorithm a in algorithms)
            {
                if (a.getName() == algorithm & a is AlgorithmWithParameters)
                {
                    AlgorithmWithParameters ap = (AlgorithmWithParameters)a;
                    ap.addParameters(parameters);
                    return a.decode(value);
                }
            }
            return null;
        }


    }
}
