﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Coder
{
    class AlgorithmLetter : Algorithm
    {
        public AlgorithmLetter(String pName)
        {
            this.name = pName;
        }

        public override string decode(String value)
        {
            return encode(value);
        }

        public override string encode(String value)
        {
            String backwardMessage = "";
            foreach (String s in value.Split(' '))
            {
                char[] charArray = s.ToCharArray();
                Array.Reverse(charArray);
                backwardMessage += new String(charArray) + " ";

            }
            return backwardMessage;
        }
    }
}
