﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Coder
{

    abstract class Editor
    {
        protected String defaultFileName = "file";
        protected String name;
        abstract public void writeFile(List<String> content);
        
        public String getName()
        {
            return this.name;
        }

        public void setdefaultFileName(String pFileName)
        {
            this.defaultFileName = pFileName;
        }

    }
}
