﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Coder
{
    class AlgorithmPhone : Algorithm
    {
        public AlgorithmPhone(String pName)
        {
            this.name = pName;
        }

        public override string decode(String value)
        {
            string[] language = { "abc", "def", "ghi", "jkl",
                "mno", "pqrs", "tuv", "wyyz" };
            string message = "";
            int x = 0;
            int y = 0;
            foreach (String s in value.Split(' '))
            {
                if (s == "*" | s == "")
                {
                    message += " ";
                }
                else
                {
                    x = (int)Char.GetNumericValue(s.First());
                    x = x - 2;
                    y = (int)Char.GetNumericValue(s.Last());
                    y = y - 1;
                    message += language.ElementAt(x).ElementAt(y);
                }
            }
            return message;
        }

        public override string encode(String value)
        {
            string[] language = { "abc", "def", "ghi", "jkl",
                "mno", "pqrs", "tuv", "wyyz" };
            string message = "";
            foreach (char c in value.ToLower())
            {
                if (c == ' ')
                {
                    message += "* ";
                }
                else
                {
                    for (int i = 0; i < language.Length; i++)
                    {
                        if (language.ElementAt(i).Contains(c))
                        {
                            message += Convert.ToString((i + 2));
                            message += Convert.ToString((language.ElementAt(i).IndexOf(c) + 1));
                            message += " ";
                        }
                    }
                }
            }

            return message;
        }
    }
}
