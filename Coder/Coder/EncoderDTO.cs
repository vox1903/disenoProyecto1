﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Coder
{
    class EncoderDTO
    {
        private List<String> fileFormats;
        private List<Tuple<String, String, String>> algorithms;
        private List<String> results;
        private String code;
        private String operation;

        public void setFileFormats(List<String> pFileFormats)
        {
            this.fileFormats = pFileFormats;
        }

        public void setAlgorithms(List<Tuple<String,String,String>> pAlgorithms)
        {
            this.algorithms = pAlgorithms;
        }

        public void setResults(List<String> pResults)
        {
            this.results = pResults;
        }

        public void setOperation(String pOperation)
        {
            this.operation = pOperation;
        }

        public void setCode(String pCode)
        {
            this.code = pCode;
        }

       
        public List<String> getFileFormats()
        {
            return this.fileFormats;
        }

        public List<Tuple<String, String,String>> getAlgorithms()
        {
            return this.algorithms;
        }

        public List<String> getResults()
        {
            return this.results;
        }

        public String getOperation()
        {
            return this.operation;
        }

        public String getCode()
        {
            return this.code;
        }

    }
}
