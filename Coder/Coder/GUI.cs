﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Coder
{
    public partial class GUI : Form
    {
        Controller controller;

        public GUI()
        {
            InitializeComponent();
            controller = new Controller();

        }

        private void groupBox2_Enter(object sender, EventArgs e)
        {

        }

        private void buttonClear_Click(object sender, EventArgs e)
        {
            richTextBoxInput.Clear();
            richTextBoxResults.Clear();
            richTextBoxResults.Text += ">";
            checkBoxBinary.Checked = false;
            checkBoxKeyWord.Checked = false;
            checkBoxLetter.Checked = false;
            checkBoxPhone.Checked = false;

            checkBoxPdf.Checked = false;
            checkBoxTxt.Checked = false;
            checkBoxXml.Checked = false;
            textBoxKeyword.Enabled = false;
            textBoxKeyword.Clear();

        }

        private void button3_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void checkBoxKeyWord_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBoxKeyWord.Checked == true)
            {
                textBoxKeyword.Enabled = true;
            }
            else if (checkBoxKeyWord.Checked == false)
            {
                textBoxKeyword.Enabled = false;
                textBoxKeyword.Clear();
            }
            if (checkBoxKeyWord.Checked == true)
            {
                comboBoxKeyWord.Enabled = true;
            }
            else if (checkBoxKeyWord.Checked == false)
            {
                comboBoxKeyWord.Enabled = false;
            }
        }

        private void buttonAccept_Click(object sender, EventArgs e)
        {
            if (checkBoxBinary.Checked == false & checkBoxKeyWord.Checked == false
                & checkBoxPhone.Checked == false & checkBoxLetter.Checked == false)
            {
                MessageBox.Show("Please check at least one algorithm.", "Warning",
                    MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            if (checkBoxKeyWord.Checked == true & textBoxKeyword.Text.Trim() == "")
            {
                MessageBox.Show("Please add a keyword for the Keyword algorithm.", "Warning",
                    MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            EncoderDTO encoderDTO = new EncoderDTO();
            encoderDTO.setCode(richTextBoxInput.Text);
            encoderDTO.setOperation("Encode and Decode");
            List<String> fileFormats = new List<String>();
            List<Tuple<String, String, String>> algorithms = new List<Tuple<String, String, String>>();
            if (checkBoxBinary.Checked == true)
                algorithms.Add(new Tuple<String, String, String>("Binary",
                    comboBoxBinary.Text,""));
            if (checkBoxKeyWord.Checked == true)
                algorithms.Add(new Tuple<String, String, String>("Keyword",
                    comboBoxKeyWord.Text, textBoxKeyword.Text));
            if (checkBoxLetter.Checked == true)
                algorithms.Add(new Tuple<String, String, String>("Letter",
                    comboBoxLetter.Text, ""));
            if (checkBoxPhone.Checked == true)
                algorithms.Add(new Tuple<String, String, String>("Phone",
                    comboBoxPhone.Text,""));

            if (algorithms.Count != 0)
            {
                if (checkBoxPdf.Checked == true)
                    fileFormats.Add("Pdf");
                if (checkBoxTxt.Checked == true)
                    fileFormats.Add("Txt");
                if (checkBoxXml.Checked == true)
                    fileFormats.Add("Xml");
                encoderDTO.setAlgorithms(algorithms);
                encoderDTO.setFileFormats(fileFormats);

                encoderDTO = controller.update(encoderDTO);
                foreach (String line in encoderDTO.getResults())
                {
                    richTextBoxResults.Text += "\n" + line;
                }
                richTextBoxResults.Text += "\n>";
            }
        }

        private void checkBoxBinary_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBoxBinary.Checked == true)
            {
                comboBoxBinary.Enabled = true;
            }
            else if (checkBoxBinary.Checked == false)
            {
                comboBoxBinary.Enabled = false;
            }
        }

        private void checkBoxLetter_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBoxLetter.Checked == true)
            {
                comboBoxLetter.Enabled = true;
            }
            else if (checkBoxLetter.Checked == false)
            {
                comboBoxLetter.Enabled = false;
            }
        }

        private void checkBoxPhone_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBoxPhone.Checked == true)
            {
                comboBoxPhone.Enabled = true;
            }
            else if (checkBoxPhone.Checked == false)
            {
                comboBoxPhone.Enabled = false;
            }
        }

        private void GUI_Load(object sender, EventArgs e)
        {
            comboBoxBinary.SelectedIndex = 0;
            comboBoxKeyWord.SelectedIndex = 0;
            comboBoxPhone.SelectedIndex = 0;
            comboBoxLetter.SelectedIndex = 0;
            comboBoxBinary.Enabled = false;
            comboBoxKeyWord.Enabled = false;
            comboBoxPhone.Enabled = false;
            comboBoxLetter.Enabled = false;
        }
    }
}
