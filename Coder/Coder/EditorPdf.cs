﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.pdf.parser;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Coder
{
    class EditorPdf : Editor
    {
       
        public EditorPdf(String pName)
        {
            this.name = pName;
            createDoc();
        }

        private void createDoc()
        {
            Document doc = new Document(PageSize.LETTER);
            PdfWriter writer = PdfWriter.GetInstance(doc,
              new FileStream(AppDomain.CurrentDomain.BaseDirectory + @"\"
               + defaultFileName + ".pdf", FileMode.Create));
            doc.AddTitle("Results");
            doc.AddCreator("Coder v1");

            doc.Open();
            Paragraph paragraph = new Paragraph();
            paragraph.Add("Solutions\n\n");
            doc.Add(paragraph);
            doc.Close();
        }

        public static string pdfText(string path)
        {
            PdfReader reader = new PdfReader(path);
            string text = string.Empty;
            for (int page = 1; page <= reader.NumberOfPages; page++)
            {
                text += PdfTextExtractor.GetTextFromPage(reader, page);
            }
            reader.Close();
            return text;
        }

        public override void writeFile(List<String> content)
        {
            String temp = pdfText(defaultFileName + ".pdf");
            Document doc = new Document(PageSize.LETTER);
            PdfWriter writer = PdfWriter.GetInstance(doc,
              new FileStream(AppDomain.CurrentDomain.BaseDirectory + @"\"
               + defaultFileName + ".pdf", FileMode.Create));
            doc.AddTitle("Results");
            doc.AddCreator("Coder v1");
            doc.Open();
            Paragraph paragraph = new Paragraph();
            paragraph.Add(temp);
            foreach (String line in content)
            {
                paragraph.Add(line);
                paragraph.Add("\n");
            }
            paragraph.Add("\n");
            paragraph.Add("\n");
            doc.Add(paragraph);
            doc.Close();

        }
        
    }
}
