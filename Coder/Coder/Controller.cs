﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Coder
{
    class Controller
    {
        private readonly String defautlLanguage = "abcdefghijklmnopqrstuvwxyz ";
        private StringEncoder stringEncoder;
        private EncoderEngineer encoderEngineer;
        private FileHandler fileHandler;


        private Language language;

        public Controller()
        {
            this.language = new Language(defautlLanguage);
            this.fileHandler = new FileHandler();
            this.encoderEngineer = new EncoderEngineer();
            this.encoderEngineer.makeEncoder(new StringEncoderBuilder());
            this.stringEncoder = this.encoderEngineer.getEncoder();
        }

        private String encode(String algorithm,String pValue)
        {
            if (language.validateWord(pValue))
            {
                return "Error";
            }
            return this.stringEncoder.encode(algorithm, pValue);
            
        }

        private String decode(String algorithm, String pValue)
        {
            if (language.validateWord(pValue))
            {
                return "Error";
            }
            return this.stringEncoder.decode(algorithm, pValue);

        }



        public EncoderDTO update(EncoderDTO request)
        {
            List<String> results;
                
            if (request.getOperation() == "Encode and Decode")
            {
                results = new List<string>();
                results.Add(System.DateTime.Now.ToString());
                results.Add("Original phrase : " + request.getCode());
                foreach (Tuple<String, String, String> algorithm in request.getAlgorithms())
                {
                    if (algorithm.Item2 == "Encode")
                    {
                        if (algorithm.Item1 == "Keyword")
                        {
                            results.Add(algorithm.Item1 + " / Encode / " +
                                this.stringEncoder.encode(algorithm.Item1, 
                                request.getCode(),algorithm.Item3));
                        }
                        else
                        {
                            results.Add(algorithm.Item1 + " / Encode / " +
                                this.stringEncoder.encode(algorithm.Item1, request.getCode()));
                        }

                        
                    }else if (algorithm.Item2 == "Decode")
                    {
                        if (algorithm.Item1 == "Keyword")
                        {
                            results.Add(algorithm.Item1 + " / Decode / " +
                                this.stringEncoder.decode(algorithm.Item1,
                                request.getCode(), algorithm.Item3));
                        }
                        else
                        {
                            results.Add(algorithm.Item1 + " / Decode / " +
                                this.stringEncoder.decode(algorithm.Item1, request.getCode()));
                        }
                    }
                }
                foreach (String fileFormat in request.getFileFormats())
                {
                    this.fileHandler.writeFile(results, fileFormat);
                }
                request.setResults(results);
                return request;



            }
            return null;
        }


    }
}
