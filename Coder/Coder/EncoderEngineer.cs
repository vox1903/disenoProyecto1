﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Coder
{
    class EncoderEngineer
    {
        private EncoderBuilder currentEncoderBuilder;

        public void makeEncoder(EncoderBuilder encoderBuilder)
        {
            this.currentEncoderBuilder = encoderBuilder;
            this.currentEncoderBuilder.buildAlgorithms();
        }

        public StringEncoder getEncoder()
        {
            return this.currentEncoderBuilder.getEncoder();  
        }


    }
}
