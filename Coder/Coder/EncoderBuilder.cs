﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Coder
{
    abstract class EncoderBuilder
    {
        protected StringEncoder currentEncoder;
        public abstract void buildAlgorithms();
        public abstract StringEncoder getEncoder();

    }
}
