﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Coder
{
    abstract class AlgorithmWithParameters : Algorithm
    {
        protected String parameters;
        public abstract void addParameters(String parameters);
    }
}
