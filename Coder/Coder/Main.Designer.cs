﻿namespace Coder
{
    partial class formMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonGUI = new System.Windows.Forms.Button();
            this.buttonConsole = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // buttonGUI
            // 
            this.buttonGUI.Location = new System.Drawing.Point(12, 12);
            this.buttonGUI.Name = "buttonGUI";
            this.buttonGUI.Size = new System.Drawing.Size(120, 33);
            this.buttonGUI.TabIndex = 0;
            this.buttonGUI.Text = "Start";
            this.buttonGUI.UseVisualStyleBackColor = true;
            this.buttonGUI.Click += new System.EventHandler(this.buttonGUI_Click);
            // 
            // buttonConsole
            // 
            this.buttonConsole.Location = new System.Drawing.Point(138, 12);
            this.buttonConsole.Name = "buttonConsole";
            this.buttonConsole.Size = new System.Drawing.Size(120, 33);
            this.buttonConsole.TabIndex = 1;
            this.buttonConsole.Text = "Start Console";
            this.buttonConsole.UseVisualStyleBackColor = true;
            this.buttonConsole.Click += new System.EventHandler(this.buttonConsole_Click);
            // 
            // formMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(289, 63);
            this.Controls.Add(this.buttonConsole);
            this.Controls.Add(this.buttonGUI);
            this.Name = "formMain";
            this.Text = "Encoder";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button buttonGUI;
        private System.Windows.Forms.Button buttonConsole;
    }
}

