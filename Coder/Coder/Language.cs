﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Coder
{
    class Language
    {
        private String structure;

        public Language(String pStructure)
        {
            structure = pStructure;
            
        }

        public void setStructure(String pStructure)
        {
            structure = pStructure;

        }

        public bool validateWord(String pWord)
        {
            foreach (char c in pWord)
            {
                if (!structure.Contains(c))
                    return false;
            }
            return true;
        }


    }
}
