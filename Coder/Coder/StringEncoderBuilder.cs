﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Coder
{
    class StringEncoderBuilder : EncoderBuilder
    {
        public override void buildAlgorithms()
        {
            List<Algorithm> algorithms = new List<Algorithm>();
            algorithms.Add(new AlgorithmBinary("Binary"));
            algorithms.Add(new AlgorithmKeyWord("Keyword"));
            algorithms.Add(new AlgorithmPhone("Phone"));
            algorithms.Add(new AlgorithmLetter("Letter"));
            this.currentEncoder = new StringEncoder(algorithms);
        }

        public override StringEncoder getEncoder()
        {
            return this.currentEncoder;
        }
    }
}
