﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Coder
{
    class Printer
    {
        private String defaultFileName;

        public void printXML(String value)
        {

        }
        public void printPdf(String value)
        {
            Document doc = new Document(PageSize.LETTER);
            PdfWriter writer = PdfWriter.GetInstance(doc,
               new FileStream(AppDomain.CurrentDomain.BaseDirectory + @"\"
                + defaultFileName + ".pdf", FileMode.Create));
            
            doc.AddTitle("Results");
            doc.AddCreator("Coder v1");
            
            doc.Open();
            doc.Add(new Paragraph(value));
            doc.Close();
        }

        public void printTxt(String value)
        {
            System.IO.File.WriteAllText(AppDomain.CurrentDomain.BaseDirectory + @"\"
                + defaultFileName + ".txt", value);
        }
    

        public void setDefaultFileName(String pDefaultFileName)
        {
            defaultFileName = pDefaultFileName;
        }

    }
}
