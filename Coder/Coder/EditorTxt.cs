﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Coder
{
    class EditorTxt : Editor
    {
        public EditorTxt(String pName)
        {
            this.name = pName;
        }


        public override void writeFile(List<String> content)
        {
            foreach (String line in content)
            {
                System.IO.File.AppendAllText(AppDomain.CurrentDomain.BaseDirectory + @"\"
                + defaultFileName + ".txt", line + "\n");
            }
        }



    }
}
