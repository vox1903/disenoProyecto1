﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Coder
{
    public partial class formMain : Form
    {
        public formMain()
        {
            InitializeComponent();
        }

        private void buttonGUI_Click(object sender, EventArgs e)
        {
            GUI gui = new GUI();
            gui.ShowDialog();
            
        }

        private void buttonConsole_Click(object sender, EventArgs e)
        {
            Console console = new Console();
            console.ShowDialog();
        }
    }
}
