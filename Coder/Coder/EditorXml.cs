﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace Coder
{
    class EditorXml : Editor
    {
        private XmlDocument xmlDoc;
        private XmlElement parentNode;

        public EditorXml(String pName)
        {
            this.name = pName;
            this.xmlDoc = new XmlDocument();
            this.parentNode = xmlDoc.CreateElement(string.Empty, "solutions", string.Empty);
            createFile();
        }

        private void createFile()
        {
            XmlDeclaration xmlDeclaration = xmlDoc.CreateXmlDeclaration("1.0", "UTF-8", null);
            XmlElement root = xmlDoc.DocumentElement;
            xmlDoc.InsertBefore(xmlDeclaration, root);

            xmlDoc.AppendChild(parentNode);
            xmlDoc.Save(defaultFileName + ".xml");
        }


        public override void writeFile(List<String> content)
        {
            String pPhrase = content.ElementAt(1).Remove(0, 18);

            XmlElement solution = xmlDoc.CreateElement(string.Empty, "solution", string.Empty);
            parentNode.AppendChild(solution);

            XmlElement date = xmlDoc.CreateElement(string.Empty, "fecha", string.Empty);
            XmlText dateValue = xmlDoc.CreateTextNode(System.DateTime.Now.ToShortDateString());
            date.AppendChild(dateValue);
            solution.AppendChild(date);

            XmlElement time = xmlDoc.CreateElement(string.Empty, "hora", string.Empty);
            XmlText timeValue = xmlDoc.CreateTextNode(System.DateTime.Now.ToShortTimeString());
            time.AppendChild(timeValue);
            solution.AppendChild(time);

            XmlElement originalPhrase = xmlDoc.CreateElement(string.Empty, "fraseOriginal", string.Empty);
            XmlText phraseValue = xmlDoc.CreateTextNode(pPhrase);
            originalPhrase.AppendChild(phraseValue);
            solution.AppendChild(originalPhrase);


            for (int i = 2; i < content.Count; i++)
            {
                String[] valuesSplit = content.ElementAt(i).Split('/');
                String pAlgoritm = valuesSplit[0];
                String pMode = valuesSplit[1];
                String pResult = valuesSplit[2]; ;

                XmlElement algorithm = xmlDoc.CreateElement(string.Empty, "Algoritmo", string.Empty);
                solution.AppendChild(algorithm);

                XmlElement algorithmName = xmlDoc.CreateElement(string.Empty, "Nombre", string.Empty);
                XmlText algorithmValue = xmlDoc.CreateTextNode(pAlgoritm);
                algorithmName.AppendChild(algorithmValue);
                algorithm.AppendChild(algorithmName);

                XmlElement mode = xmlDoc.CreateElement(string.Empty, "Modo", string.Empty);
                XmlText modeValue = xmlDoc.CreateTextNode(pMode);
                mode.AppendChild(modeValue);
                algorithm.AppendChild(mode);

                XmlElement result = xmlDoc.CreateElement(string.Empty, "Resultado", string.Empty);
                XmlText resultValue = xmlDoc.CreateTextNode(pResult);
                result.AppendChild(resultValue);
                algorithm.AppendChild(result);

            }

            xmlDoc.Save(defaultFileName + ".xml");
        }
    }
}
