﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Coder
{
    class AlgorithmKeyWord : AlgorithmWithParameters
    {
        public AlgorithmKeyWord(String pName)
        {
            this.name = pName;
        }

        int mod(int x, int m)
        {
            return (x % m + m) % m;
        }

        public override string decode(String value)
        {
            
            String message = "";
            string language = "abcdefghijklmnopqrstuvwxyz";

            foreach (String s in value.ToLower().Split(' '))
            {
                for (int i = 0; i < s.Length; i++)
                {
                    int w = language.IndexOf(s.ElementAt(i));
                    int k = language.IndexOf(this.parameters.ElementAt(i % this.parameters.Length));
                    message += language.ElementAt(mod((w - k - 1), language.Length));
                }
                message += " ";
            }
            return message;

        }

        public override string encode(String value)
        {
            String message = "";
            string language = "abcdefghijklmnopqrstuvwxyz";

            foreach (String s in value.ToLower().Split(' '))
            {
                for (int i = 0; i < s.Length; i++)
                {
                    int w = language.IndexOf(s.ElementAt(i));
                    int k = language.IndexOf(this.parameters.ElementAt(i % this.parameters.Length));
                    message += language.ElementAt((w + k + 1) % language.Length);
                }
                message += " ";
            }

            return message;
        }

        public override void addParameters(string parameters)
        {
            this.parameters = parameters;
        }
    }
}
