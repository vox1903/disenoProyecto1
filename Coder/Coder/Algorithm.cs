﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Coder
{
    abstract class Algorithm
    {
        protected String name;
        public abstract String encode(String value);
        public abstract String decode(String value);

        public String getName()
        {
            return name;
        }

    }
}
