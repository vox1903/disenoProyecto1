﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Coder
{
    public partial class Console : Form
    {
        Controller controller;        

        public Console()
        {
            InitializeComponent();
            controller = new Controller();
        }



        /*
            code "tarea programada" -a -e Binary -a -d Letter -f Pdf
            var regex = @"^code\s\w*((\s-a\s(-d|-e)\s\w*)+)(\s-f\s\w*)*$";
            var match = Regex.Match(textBoxInput.Text,
                regex, RegexOptions.IgnoreCase);
            */

        private void buttonSend_Click(object sender, EventArgs e)
        {
            
            richTextBoxResults.Text += textBoxInput.Text + "\n";
            richTextBoxResults.Text += ">>";
            var regex = @"^code\s"+'"'+"(\\w*\\s*)*"+'"'+"(\\s-a\\s(-d|-e)\\s(\\w*|Keyword\\s\\w*))+(\\s-f\\s\\w*)*$";
            var match = Regex.Match(textBoxInput.Text,
                regex, RegexOptions.IgnoreCase);

            if (!match.Success)
            {
                richTextBoxResults.Text += ">Syntax error on code\n";
                richTextBoxResults.Text += ">>";
                return;
            }
            
            EncoderDTO encoderDTO = new EncoderDTO();

            if (textBoxInput.Text.Substring(0, textBoxInput.Text.IndexOf(' ')) == "code")
            {
                String code = textBoxInput.Text.Split('"')[1];
                List<String> entry = textBoxInput.Text.Split('"')[2].Substring(1).Split(' ').ToList();
                List<Tuple<string, string, string>> algorithms = new List<Tuple<string, string, string>>();
                List<String> fileFormats = new List<string>();

                encoderDTO.setOperation("Encode and Decode");
                encoderDTO.setCode(code);
                int i = 0;
                while (i < entry.Count)
                {
                    //ALGORITHMS
                    if (entry.ElementAt(i) == "-a")
                    {
                        i++;
                        if (entry.ElementAt(i) == "-e")
                        {
                            i++;
                            if (entry.ElementAt(i) == "Keyword")
                            {
                                algorithms.Add(new Tuple<string, string, string>(entry.ElementAt(i),
                                    "Encode", entry.ElementAt(i+1)));
                                i = i + 2;
                            }
                            else
                            {
                                algorithms.Add(new Tuple<string, string, string>(entry.ElementAt(i),
                                    "Encode", null));
                                i++;
                            }
                        }
                        else if (entry.ElementAt(i) == "-d")
                        {
                            i++;
                            if (entry.ElementAt(i) == "Keyword")
                            {
                                algorithms.Add(new Tuple<string, string, string>(entry.ElementAt(i),
                                    "Decode", entry.ElementAt(i + 1)));
                                i = +2;
                            }
                            else
                            {
                                algorithms.Add(new Tuple<string, string, string>(entry.ElementAt(i),
                                    "Decode", null));
                                i++;
                            }
                        }
                        
                    }else if (entry.ElementAt(i) == "-f")
                    {
                        i++;
                        fileFormats.Add(entry.ElementAt(i));
                        i++;

                    }
                    else {
                        richTextBoxResults.Text += ">Syntax error on code\n";
                        richTextBoxResults.Text += ">>";
                        return; }

                }
                encoderDTO.setAlgorithms(algorithms);
                encoderDTO.setFileFormats(fileFormats);
                encoderDTO = controller.update(encoderDTO);
                foreach (String line in encoderDTO.getResults())
                {
                    richTextBoxResults.Text += "\n" + line;
                }
                richTextBoxResults.Text += "\n>>";
            }
            else
            {
                richTextBoxResults.Text += ">Syntax error\n";
                richTextBoxResults.Text += ">>";
            }

            textBoxInput.Clear();
        }

        private void Console_Load(object sender, EventArgs e)
        {
            //code "tarea programada" -a -e Binary -a -d Letter -f Pdf
            richTextBoxResults.Text += ">Welcome!\n";
            
            richTextBoxResults.Text += ">Type -help for help\n";
            richTextBoxResults.Text += ">Type code "+'"'+ "phrase" +'"'+" -a -e Algorithm -f FileFormat\n";
            richTextBoxResults.Text += ">Example";
            richTextBoxResults.Text += ">code " + '"' + "tarea programada" + '"' + " -a -e Binary -a -d Letter -f Pdf\n";
            richTextBoxResults.Text += ">>";
        }
    }
}
